package com.chinasie.barcodescanplugin;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.WriterException;
import com.zxing.activity.CaptureActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAB = MainActivity.class.getSimpleName();
    //显示扫描结果
    private EditText editText = null;
    //扫描按钮
    private Button btnScan = null;
    //普通图片
    private ImageView imageNormal = null;
    //普通按钮
    private Button buttonNormal = null;
    //特按钮
    private Button buttonAndCenter = null;
    //有中心图片
    private ImageView imageWithCenter = null;
    private RequestQueue requestQueue;
    private Gson gson;
    private StringRequest stringRequest;
    private String obj;
    private Object result;
    private Uri imageFileUri;
    private List<CarRecord> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestQueue = Volley.newRequestQueue(this);
        gson = new Gson();
        initView();

    }

    //    https://api.weibo.com/2/short_url/shorten.json?source=5786724301&url_long=http://www.baidu.com&url_long=http://www.sina.com.cn
    private void getShortUrl(final String obj) {

        //在这里设置需要post的参数
        StringRequest request = new StringRequest(Request.Method.POST, "https://api.weibo.com/2/short_url/shorten.json", new Response.Listener<String>() {
            @Override
            public void onResponse(final String s) {

                final String filePath = File.separator
                        + "qr_" + System.currentTimeMillis() + ".jpg";
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    String urls = jsonObject.getString("urls");
                    list = gson.fromJson(urls, new TypeToken<List<CarRecord>>() {
                    }.getType());


                    //二维码图片较大时，生成图片、保存文件的时间可能较长，因此放在新线程中
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

//                                success.setPixel(10,10,R.color.colorPrimaryDark);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (list != null) {
                                        for (int i = 0; i < list.size(); i++) {

                                            String shortUrl = list.get(i).getUrl_short();
                                            Toast.makeText(MainActivity.this, shortUrl + "", Toast.LENGTH_SHORT).show();
                                            setBiMap(filePath,shortUrl);
                                        }

                                        //带有中心图片的二维码显示在 imageWithCenter 上
//                                            imageWithCenter.setImageBitmap(success);
//                                            imageWithCenter.setBackgroundResource(R.drawable.fillet);

                                    }

                                }
                            });
                        }
                    }).start();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(MainActivity.this, "网络连接失败！", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("source", "5786724301");
                map.put("url_long", obj);
                return map;
            }
        };
        requestQueue.add(request);
    }

//    private void setPerson() {
//
//        Person person = new Person("xxxx", "24");
//        ArrayList<Person> listP = new ArrayList<Person>();
//        listP.add(person);
//        listP.add(person);
//
//        System.out.println(person.getName());
//        Gson json = new Gson();
//        Type type = new TypeToken<ArrayList<Person>>() {
//        }.getType();
//        result = json.toJson(listP, type);
//    }


    public void getHttp() {

        //在这里设置需要post的参数
        StringRequest request = new StringRequest(Request.Method.POST, "你的Url", new Response.Listener<String>() {
            @Override
            public void onResponse(final String s) {

                try {
                    JSONObject jsonObject = new JSONObject(s);
                    String msgType = jsonObject.getString("msgType");
                    String msg = jsonObject.getString("msg");
                    boolean success = jsonObject.getBoolean("success");
                    if ("1".equals(msgType) && true == success) {
                        System.out.print(s + "二维码");

                        obj = jsonObject.getString("obj");
                        getShortUrl(obj);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(MainActivity.this, "网络连接失败！", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("agentId", "123232123123123312323312323112332231123231312323123232311231");
                return map;
            }
        };
        requestQueue.add(request);
    }

    private void setBiMap(String path, String filePath) {


        try {
            LogoConfig logoConfig = new LogoConfig();

            Bitmap logoBitmap = logoConfig.modifyLogo(
                    BitmapFactory.decodeResource(getResources(),
                            R.drawable.gg), BitmapFactory
                            .decodeResource(getResources(),
                                    R.drawable.gg));
            Bitmap codeBitmap = QRCodeUtil.createCode(filePath, logoBitmap);
            saveBitmap(codeBitmap, "guolin_code.png");
            imageWithCenter.setImageBitmap(codeBitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }

    }


    /**
     * 保存生成的二维码图片
     */
    private void saveBitmap(Bitmap bitmap, String bitName) {
        //获取与应用相关联的路径
        String imageFilePath = getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        File imageFile = new File(imageFilePath, "/" + bitName);// 通过路径创建保存文件
        imageFileUri = Uri.fromFile(imageFile);
        if (imageFile.exists()) {
            imageFile.delete();
        }
        FileOutputStream out;
        try {
            out = new FileOutputStream(imageFile);
            if (bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)) {
                out.flush();
                out.close();
            }
        } catch (Exception e) {
        }
    }

    /**
     * 点击时间响应
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnScan:
                try {
                    //打开扫描界面扫描条形码或二维码
                    Intent openCameraIntent = new Intent(MainActivity.this, CaptureActivity.class);
                    startActivityForResult(openCameraIntent, 0);
                } catch (Exception ex) {
                    Log.e(TAB, ex.getMessage());
                    ex.printStackTrace();
                }
                break;
            case R.id.button:
                //普通的二维码图片显示到一个ImageView上面
                imageNormal.setImageBitmap(QRCodeUtil.createQRImage("123456789"));
                break;
            case R.id.buttonAndCenter:
                getHttp();
                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //处理扫描结果（在界面上显示）
        if (resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("result");
            editText.setText(scanResult);
        }
    }

    /**
     * 初始化View
     */
    private void initView() {
        editText = (EditText) this.findViewById(R.id.editText);
        imageWithCenter = (ImageView) findViewById(R.id.imageAndCenter);
        imageNormal = (ImageView) this.findViewById(R.id.image);

        btnScan = (Button) this.findViewById(R.id.btnScan);
        buttonNormal = (Button) this.findViewById(R.id.button);
        buttonAndCenter = (Button) findViewById(R.id.buttonAndCenter);


        btnScan.setOnClickListener(this);
        buttonNormal.setOnClickListener(this);
        buttonAndCenter.setOnClickListener(this);
    }
}
