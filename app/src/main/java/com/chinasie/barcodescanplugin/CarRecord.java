package com.chinasie.barcodescanplugin;

/**
 * Created by fgtty on 2017/8/16.
 */

public class CarRecord {

//    "object_type": "",
//            "result": true,
//            "url_short": "http://t.cn/RC7IxmX",
//            "object_id": "",
//            "url_long": "http://sv.ruyichuxing.com/rycx/passengerApi/promotedTweets.htm",
//            "type": 0

    private String object_type;
    private Boolean result;
    private String url_short;
    private String object_id;
    private String url_long;
    private int type;

    public String getObject_type() {
        return object_type;
    }

    public void setObject_type(String object_type) {
        this.object_type = object_type;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getUrl_short() {
        return url_short;
    }

    public void setUrl_short(String url_short) {
        this.url_short = url_short;
    }

    public String getObject_id() {
        return object_id;
    }

    public void setObject_id(String object_id) {
        this.object_id = object_id;
    }

    public String getUrl_long() {
        return url_long;
    }

    public void setUrl_long(String url_long) {
        this.url_long = url_long;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public CarRecord(String object_type, Boolean result, String url_short, String object_id, String url_long, int type) {
        this.object_type = object_type;
        this.result = result;
        this.url_short = url_short;
        this.object_id = object_id;
        this.url_long = url_long;
        this.type = type;
    }
}
